﻿Feature: CreditPage
	As a user
	I want to go on the credit page
	In order to read information about credits

Background: 
	Given Allo page is opened

Scenario: Go on the blog page
	When I click on the credit button
	Then Credit page is opened
	Then I see information about credits