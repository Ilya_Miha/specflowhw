﻿Feature: GarantyPage
	As a user
	I want to go on the guarantee page
	In order to read information about purchase returns

Background: 
		Given Main page is opened

Scenario: Go to the guarantee page
	When I click on the guarantee button
	Then Guarantee page is opened
	Then I see information about guarantee conditions