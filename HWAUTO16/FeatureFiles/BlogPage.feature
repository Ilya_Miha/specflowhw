﻿Feature: BlogPage
	
	As a user
	I want to go on the blog page
	In order to explore the latest technology news


Scenario: Go to the blog page
	Given Allo page is opened
	When I click on the blog button
	Then Blog page opened
	Then I see information about technology