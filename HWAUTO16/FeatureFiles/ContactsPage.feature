﻿Feature: ContactsPage

	As a user
	I want to go to the contacts page
	In order to read information about contacts

	As a user
	I want to send my сontact details on the site
	In order to get feedback

Background: 
	Given Allo page is opened


Scenario: Go to the contacts page
	When I click on the contacts button
	Then Contacts page is opened
	Then I see information about contacts

Scenario: Succese sendind message on the contact page
	When I click on the contacts page button
	When I fill name field 'Тест'
	When I fill Email field 'test@i.ua'
	When I fill message field 'Allo the best - no'
	When I click on the message button
	Then I see information about succese message sending
