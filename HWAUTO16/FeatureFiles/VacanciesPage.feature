﻿Feature: VacanciesPage
	As a user
	I want to go to the vacancies page
	In order to read information about employment

Background: 
	Given Allo page is opened

Scenario: Go to the vacancies page
	When I click on the vacancies button
	Then Vacancies page is opened
	Then I see information about vacancies conditions