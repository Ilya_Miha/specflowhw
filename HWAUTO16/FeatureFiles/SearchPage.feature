﻿Feature: SearchPage
	As a user
	I want to enter the name of product into search field
	In order to find produсе I`m interested in

Background: 
	Given Allo page is opened

Scenario: Search of the product
	When I fill search field 'IPhone 11'
	When I click on the search button
	Then I see information about result of the search
	Then I see products I`m interested in