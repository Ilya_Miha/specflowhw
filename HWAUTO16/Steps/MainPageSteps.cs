﻿using HWAUTO16.POM;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class MainPageSteps
    {
        public MainPage mainPage;
        
        [When(@"I click on the authorization form")]
        public void WhenIClickOnTheAuthorizationForm()
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            mainPage.ClickOnAuthForm();
            mainPage.ClickLogin();
        }
        
        [When(@"I fill email field '(.*)'")]
        public void WhenIFillEmailField(string p0)
        {
            mainPage.EnterTextToEmail("ilya.miha1337@gmail.com");
        }
        
        [When(@"I fill password field '(.*)'")]
        public void WhenIFillPasswordField(string p0)
        {
            mainPage.EnterTextToPassword("08122106i");
        }
        
        [When(@"I click Login button")]
        public void WhenIClickLoginButton()
        {
            mainPage.ClickOnFormButton();
        }
        
        [Then(@"I see authorization form is opened")]
        public void ThenISeeAuthorizationFormIsOpened()
        {
            string authTitle = mainPage.GexTextFormTitle();
            Assert.AreEqual("ВХІД", authTitle);
        }
        
        [Then(@"I see my username on the main page")]
        public void ThenISeeMyUsernameOnTheMainPage()
        {
            string userName = mainPage.GexTextUserTitle();
            Assert.AreEqual("Илья", userName);
        }
    }
}
