﻿using HWAUTO16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class VacanciesPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public VacanciesPage vacanciesPage;

        [Before]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\Users\ilyam\Desktop\Practices\ChromeDriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            mainPage = new MainPage(driver);
            vacanciesPage = new VacanciesPage(driver);
        }

        [Given(@"Site main page is opened")]
        public void GivenSiteMainPageIsOpened()
        {
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
        }
        
        [When(@"I click on the vacancies button")]
        public void WhenIClickOnTheVacanciesButton()
        {
            vacanciesPage = mainPage.ClickOnVacanciesButton();
        }
        
        [Then(@"Vacancies page is opened")]
        public void ThenVacanciesPageIsOpened()
        {
            string vacanciesTitle = vacanciesPage.GexTextVacanciesTitle();
            Assert.AreEqual("Робота в АЛЛО - Тиць, і ти вже з нами", vacanciesTitle);
        }
        
        [Then(@"I see information about vacancies conditions")]
        public void ThenISeeInformationAboutVacanciesConditions()
        {
            string vacanciesInfo = vacanciesPage.GetTextVacanciesInfo();
            Assert.AreEqual("Тиць, і ти вже з нами", vacanciesInfo);
        }

        [After]
        public void KillDriver()
        {
            driver.Quit();
        }
    }
}
