﻿using HWAUTO16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class GarantyPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public GarantyPage garantyPage;

        [Before]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\Users\ilyam\Desktop\Practices\ChromeDriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            mainPage = new MainPage(driver);
            garantyPage = new GarantyPage(driver);
        }

        [Given(@"Main page is opened")]
        public void GivenMainPageIsOpened()
        {
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
        }

        [When(@"I click on the guarantee button")]
        public void WhenIClickOnTheGuaranteeButton()
        {
            garantyPage = mainPage.ClickOnGarantyButton();
        }
        
        [Then(@"Guarantee page is opened")]
        public void ThenGuaranteePageIsOpened()
        {
            string garantyTitle = garantyPage.GexTextGarantyTitle();
            Assert.AreEqual("Гарантiя та Сервісні центри", garantyTitle);
        }
        
        [Then(@"I see information about guarantee conditions")]
        public void ThenISeeInformationAboutGuaranteeConditions()
        {
            string garantyInfo = garantyPage.GetTextGarantyInfo();
            Assert.AreEqual("Гарантійне обслуговування", garantyInfo);
        }

        [After]
        public void KillDriver()
        {
            driver.Quit();
        }
    }
}
