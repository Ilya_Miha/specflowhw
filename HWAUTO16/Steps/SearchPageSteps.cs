﻿using HWAUTO16.POM;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class SearchPageSteps
    {
        public MainPage mainPage;
        public SearchePage searchPage;

        [When(@"I fill search field '(.*)'")]
        public void WhenIFillSearchField(string p0)
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            mainPage.EnterTextInSearchField("IPhone 11");
        }
        
        [When(@"I click on the search button")]
        public void WhenIClickOnTheSearchButton()
        {
            searchPage = mainPage.ClickOnSearchButton();
        }
        
        [Then(@"I see information about result of the search")]
        public void ThenISeeInformationAboutResultOfTheSearch()
        {
            string searchResult = searchPage.GexTextSearcheTitle();
            Assert.AreEqual("Результати пошуку для 'IPhone 11'. Знайдено товарів: 16532", searchResult);
        }
        
        [Then(@"I see products I`m interested in")]
        public void ThenISeeProductsIMInterestedIn()
        {
            string productTitle = searchPage.GetTextProductCard();
            Assert.AreEqual("Apple iPhone 11 128GB Black (MHDH3) Slim Box", productTitle);
        }
    }
}
