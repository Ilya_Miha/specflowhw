﻿using HWAUTO16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class ContactsPageSteps
    {
        public IWebDriver driver;
        public MainPage mainPage;
        public ContactsPage contactsPage;


        [When(@"I click on the contacts page button")]
        public void WhenIClickOnTheContactsPageButton()
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            contactsPage = mainPage.ClickOnContactsButton();
        }


        [When(@"I click on the contacts button")]
        public void WhenIClickOnTheContactsButton()
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            contactsPage = mainPage.ClickOnContactsButton();
        }
        
        [When(@"I fill name field '(.*)'")]
        public void WhenIFillNameField(string p0)
        {
            contactsPage.EnterTextToName("Тест");
        }
        
        [When(@"I fill Email field '(.*)'")]
        public void WhenIFillEmailField(string p0)
        {
            contactsPage.EnterTextToEmail("test@i.ua");
        }
        
        [When(@"I fill message field '(.*)'")]
        public void WhenIFillMessageField(string p0)
        {
            contactsPage.EnterTextToMessage("Allo the best - no");
        }
        
        [When(@"I click on the message button")]
        public void WhenIClickOnTheMessageButton()
        {
            contactsPage.ClickOnFormButton();
        }
        
        [Then(@"Contacts page is opened")]
        public void ThenContactsPageIsOpened()
        {
            string contactsTitle = contactsPage.GexTextContactsTitle();
            Assert.AreEqual("Консультації та замовлення за телефонами", contactsTitle);
        }
        
        [Then(@"I see information about contacts")]
        public void ThenISeeInformationAboutContacts()
        {
            string contactsInfo = contactsPage.GetTextContactsInfo();
            Assert.AreEqual("Безкоштовно по Україні:", contactsInfo);
        }
        
        [Then(@"I see information about succese message sending")]
        public void ThenISeeInformationAboutSucceseMessageSending()
        {
            string succeseMessage = contactsPage.GexTextMessageTitle();
            Assert.AreEqual("Ваше повідомлення надіслано в обробку.", succeseMessage);
        }
    }
}
