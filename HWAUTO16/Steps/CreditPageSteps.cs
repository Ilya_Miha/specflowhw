﻿using HWAUTO16.POM;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class CreditPageSteps
    {
        public MainPage mainPage;
        public CreditPage creditPage;

        [When(@"I click on the credit button")]
        public void WhenIClickOnTheCreditButton()
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            creditPage = mainPage.ClickOnCreditButton();
        }
        
        [Then(@"Credit page is opened")]
        public void ThenCreditPageIsOpened()
        {
            string creditTitle = creditPage.GexTextCreditTitle();
            Assert.AreEqual("Кредит", creditTitle);
        }
        
        [Then(@"I see information about credits")]
        public void ThenISeeInformationAboutCredits()
        {
            string creditInfo = creditPage.GetTextCreditInfo();
            Assert.AreEqual("Ознайомтеся з умовами, які пропонують наші Банки-партнери і робіть покупки прямо зараз!", creditInfo);
        }
    }
}
