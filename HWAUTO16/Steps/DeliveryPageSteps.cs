﻿using HWAUTO16.POM;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class DeliveryPageSteps
    {
        public MainPage mainPage;
        public DeliveryPage deliveryPage;

        [When(@"I click on the delivery button")]
        public void WhenIClickOnTheDeliveryButton()
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            deliveryPage = mainPage.ClickOnDeliveryButton();
        }
        
        [Then(@"Delivery page is opened")]
        public void ThenDeliveryPageIsOpened()
        {
            string deliveryTitle = deliveryPage.GexTextDeliveryTitle();
            Assert.AreEqual("Доставка і оплата", deliveryTitle);
        }
        
        [Then(@"I see see information about delivery")]
        public void ThenISeeSeeInformationAboutDelivery()
        {
            string deliveryInfo = deliveryPage.GetTextDeliveryInfo();
            Assert.AreEqual("Як оформити замовлення?", deliveryInfo);
        }
    }
}
