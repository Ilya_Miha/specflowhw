﻿using HWAUTO16.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class BlogPageSteps
    {
        public static IWebDriver driver;
        public static MainPage mainPage;
        public static BlogPage blogPage;

        [Before]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\Users\ilyam\Desktop\Practices\ChromeDriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            mainPage = new MainPage(driver);
            blogPage = new BlogPage(driver);
        }

        [Given(@"Allo page is opened")]
        public void GivenAlloPageIsOpened()
        {
            driver.Navigate().GoToUrl("https://allo.ua/");
            driver.Manage().Window.Maximize();
        }

        [When(@"I click on the blog button")]
        public void WhenIClickOnTheBlogButton()
        {
            blogPage = mainPage.ClickOnButtonBlog();
        }

        [Then(@"Blog page opened")]
        public void ThenBlogPageOpened()
        {
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            string blogTitle = blogPage.GexTextBlogTitle();
            Assert.AreEqual("НОВИНИ", blogTitle);
        }

        [Then(@"I see information about technology")]
        public void ThenISeeInformationAboutTechnology()
        {
            string blogInfo = blogPage.GetTextBlogInfo();
            Assert.AreEqual("Збираємо потужний ігровий ПК", blogInfo);
        }

        [After]
        public void KillDriver()
        {
            driver.Quit();
        }
    }
}
