﻿using HWAUTO16.POM;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace HWAUTO16.Steps
{
    [Binding]
    public class ShopsPageSteps
    {
        public MainPage mainPage;
        public ShopsPage shopsPage;

        [When(@"I click on the shops button")]
        public void WhenIClickOnTheShopsButton()
        {
            mainPage = new MainPage(BlogPageSteps.driver);
            shopsPage = mainPage.ClickOnShopsButton();
        }
        
        [Then(@"Shops page is opened")]
        public void ThenShopsPageIsOpened()
        {
            string shopsTitle = shopsPage.GexTextShopsTitle();
            Assert.AreEqual("Адреси магазинів", shopsTitle);
        }
        
        [Then(@"I see see addresses of the shops")]
        public void ThenISeeSeeAddressesOfTheShops()
        {
            string shopsInfo = shopsPage.GetTextShopsInfo();
            Assert.AreEqual("Введіть ваше місто:", shopsInfo);
        }
    }
}
