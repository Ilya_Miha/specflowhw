﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class BlogPage
    {
        private IWebDriver _driver;

        public By getBlogTitle = By.XPath("/html/body/div[2]/header/div/div/div/div/div[2]/div[2]/nav/ul/li[1]/a");
        public By getBlogInfo = By.XPath("/html/body/div[2]/div[2]/div[2]/div/div/div/div[2]/div/div/div/div/div[1]/article/div/div[2]/div[1]/header/h3/a");

        public BlogPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindBlogTitle()
        {
            return _driver.FindElement(getBlogTitle);
        }

        public string GexTextBlogTitle()
        {
            return FindBlogTitle().Text;
        }

        public IWebElement FindBlogInfo()
        {
            return _driver.FindElement(getBlogInfo);
        }

        public string GetTextBlogInfo()
        {
            return FindBlogInfo().Text;
        }
    }
}
