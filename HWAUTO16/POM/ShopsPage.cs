﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class ShopsPage
    {
        private IWebDriver _driver;

        public By getShopsTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");
        public By getShopsInfo = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/div/label");

        public ShopsPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindShopsTitle()
        {
            return _driver.FindElement(getShopsTitle);
        }

        public string GexTextShopsTitle()
        {
            return FindShopsTitle().Text;
        }

        public IWebElement FindShopsInfo()
        {
            return _driver.FindElement(getShopsInfo);
        }

        public string GetTextShopsInfo()
        {
            return FindShopsInfo().Text;
        }
    }
}
