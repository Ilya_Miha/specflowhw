﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class MainPage
    {
        private IWebDriver _driver;

        public By getBlogPage = By.ClassName("info1_");
        public By getGarantyPage = By.ClassName("info1_garantija");
        public By getVacanciesPage = By.ClassName("info1_vac");
        public By getShopsPage = By.XPath("/html/body/div[1]/div/div/div[1]/div/div[1]/div/div[3]/ul/li[4]/a");
        public By getDeliveryPage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By getCreditPage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By getContactsPage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");

        public By getTitleAuthForm = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[1]");
        public By getAuthForm = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div");
        public By getInputEmail = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By getInputPassword = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
        public By getFormButton = By.XPath("//html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
        public By getUserName = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/div[1]/span");

        public By getSearchName = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By getSearchButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/button[2]");

        public MainPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public BlogPage ClickOnButtonBlog()
        {
            _driver.FindElement(getBlogPage).Click();
            return new BlogPage(_driver);
        }
        public GarantyPage ClickOnGarantyButton()
        {
            _driver.FindElement(getGarantyPage).Click();
            return new GarantyPage(_driver);
        }

        public VacanciesPage ClickOnVacanciesButton()
        {
            _driver.FindElement(getVacanciesPage).Click();
            return new VacanciesPage(_driver);
        }

        public ShopsPage ClickOnShopsButton()
        {
            _driver.FindElement(getShopsPage).Click();
            return new ShopsPage(_driver);
        }

        public DeliveryPage ClickOnDeliveryButton()
        {
            _driver.FindElement(getDeliveryPage).Click();
            return new DeliveryPage(_driver);
        }

        public CreditPage ClickOnCreditButton()
        {
            _driver.FindElement(getCreditPage).Click();
            return new CreditPage(_driver);
        }

        public ContactsPage ClickOnContactsButton()
        {
            _driver.FindElement(getContactsPage).Click();
            return new ContactsPage(_driver);
        }

        public MainPage ClickOnAuthForm()
        {
            _driver.FindElement(getAuthForm).Click();
            return this;
        }

        public MainPage EnterTextToEmail(string name)
        {
            _driver.FindElement(getInputEmail).SendKeys(name);
            return this;
        }

        public MainPage EnterTextToPassword(string password)
        {
            _driver.FindElement(getInputPassword).SendKeys(password);
            return this;
        }

        public MainPage ClickOnFormButton()
        {
            _driver.FindElement(getFormButton).Click();
            return this;
        }

        public IWebElement FindUserName()
        {
            return _driver.FindElement(getUserName);
        }

        public string GexTextUserTitle()
        {
            return FindUserName().Text;
        }

        public IWebElement FindFormTitle()
        {
            return _driver.FindElement(getTitleAuthForm);
        }

        public string GexTextFormTitle()
        {
            return FindFormTitle().Text;
        }

        public MainPage ClickLogin()
        {
            _driver.FindElement(getTitleAuthForm).Click();
            return this;
        }

        public MainPage EnterTextInSearchField(string name)
        {
            _driver.FindElement(getSearchName).SendKeys(name);
            return this;
        }

        public SearchePage ClickOnSearchButton()
        {
            _driver.FindElement(getSearchButton).Click();
            return new SearchePage(_driver);
        }
    }
}
