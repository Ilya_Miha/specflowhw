﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class ContactsPage
    {
        private IWebDriver _driver;

        public By getContactsTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");
        public By getContactsInfo = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[1]/h5");

        public By getInputName = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[1]/div/div/input");
        public By getInputEmail = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[2]/div/div[2]/div/input");
        public By getInputMessage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/div[4]/div/div/textarea");
        public By getFormButton = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/form/button");
        public By getSuccessMessage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div/div/p");

        public ContactsPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindContactsTitle()
        {
            return _driver.FindElement(getContactsTitle);
        }

        public string GexTextContactsTitle()
        {
            return FindContactsTitle().Text;
        }

        public IWebElement FindMessageTitle()
        {
            return _driver.FindElement(getSuccessMessage);
        }

        public string GexTextMessageTitle()
        {
            return FindMessageTitle().Text;
        }

        public IWebElement FindContactsInfo()
        {
            return _driver.FindElement(getContactsInfo);
        }

        public string GetTextContactsInfo()
        {
            return FindContactsInfo().Text;
        }

        public ContactsPage EnterTextToEmail(string email)
        {
            _driver.FindElement(getInputEmail).SendKeys(email);
            return this;
        }

        public ContactsPage EnterTextToName(string name)
        {
            _driver.FindElement(getInputName).SendKeys(name);
            return this;
        }

        public ContactsPage EnterTextToMessage(string message)
        {
            _driver.FindElement(getInputMessage).SendKeys(message);
            return this;
        }

        public ContactsPage ClickOnFormButton()
        {
            _driver.FindElement(getFormButton).Click();
            return this;
        }
    }
}
