﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class VacanciesPage
    {
        private IWebDriver _driver;

        public By getVacanciesTitle = By.XPath("/html/head/title");
        public By getVacanciesInfo = By.XPath("/html/body/div[2]/div[3]/div/div/div/div/article/div/div[1]/div/div/div[1]/div/h2");

        public VacanciesPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindVacanciesTitle()
        {
            return _driver.FindElement(getVacanciesTitle);
        }

        public string GexTextVacanciesTitle()
        {
            return FindVacanciesTitle().GetAttribute("text");
        }

        public IWebElement FindVacanciesInfo()
        {
            return _driver.FindElement(getVacanciesInfo);
        }

        public string GetTextVacanciesInfo()
        {
            return FindVacanciesInfo().Text;
        }
    }
}
