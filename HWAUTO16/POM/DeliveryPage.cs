﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class DeliveryPage
    {
        private IWebDriver _driver;

        public By getDeliveryTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");
        public By getDeliveryInfo = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div[2]/div[3]/div/h3");

        public DeliveryPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindDeliveryTitle()
        {
            return _driver.FindElement(getDeliveryTitle);
        }

        public string GexTextDeliveryTitle()
        {
            return FindDeliveryTitle().Text;
        }

        public IWebElement FindDeliveryInfo()
        {
            return _driver.FindElement(getDeliveryInfo);
        }

        public string GetTextDeliveryInfo()
        {
            return FindDeliveryInfo().Text;
        }
    }
}
