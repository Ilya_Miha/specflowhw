﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class SearchePage
    {
        private IWebDriver _driver;

        public By getSearcheResult = By.XPath("/html/body/div[1]/div/div/div[2]/ul/li[2]/span");
        public By getProductCard = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[2]/div[3]/div[1]/div/div[3]/a");

        public SearchePage(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindSearcheTitle()
        {
            return _driver.FindElement(getSearcheResult);
        }

        public string GexTextSearcheTitle()
        {
            return FindSearcheTitle().Text;
        }

        public IWebElement FindProductCard()
        {
            return _driver.FindElement(getProductCard);
        }

        public string GetTextProductCard()
        {
            return FindProductCard().Text;
        }
    }
}
