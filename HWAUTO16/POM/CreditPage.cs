﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWAUTO16.POM
{
    public class CreditPage
    {
        private IWebDriver _driver;

        public By getCreditTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");
        public By getCreditInfo = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h3[1]");

        public CreditPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindCreditTitle()
        {
            return _driver.FindElement(getCreditTitle);
        }

        public string GexTextCreditTitle()
        {
            return FindCreditTitle().Text;
        }

        public IWebElement FindCreditInfo()
        {
            return _driver.FindElement(getCreditInfo);
        }

        public string GetTextCreditInfo()
        {
            return FindCreditInfo().Text;
        }
    }
}
